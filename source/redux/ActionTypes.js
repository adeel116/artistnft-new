// Auth types
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const AUTH_LOADING = 'AUTH_LOADING';
export const SAVE_REGISTER_DATA = 'SAVE_REGISTER_DATA';

// Utils types
export const UTILS_LOADING = 'UTILS_LOADING';
export const GET_ALL_ROLES = 'GET_ALL_ROLES';

// Relogin MISSCALL types
export const MISSED_CALL = 'MISSED_CALL';
export const VERIFY_NUMBER_REQUESTED = 'VERIFY_NUMBER_REQUESTED';
export const VERIFY_NUMBER_SUCCEED = 'VERIFY_NUMBER_SUCCEED';
export const VERIFY_NUMBER_FAILED = 'VERIFY_NUMBER_FAILED';
