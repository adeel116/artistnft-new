import React from 'react'
import { View, Text, TouchableWithoutFeedback } from 'react-native'
import styles from './styles'

export default function OutlinedButton({ title, onPress, style, titleStyle }) {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={[styles.outlinedContainer, style]}>
                <Text style={[styles.title, titleStyle]}>{title}</Text>
            </View>
        </TouchableWithoutFeedback>
    )
}
