import React from 'react'
import { View, Image } from 'react-native'
import styles from './styles'
import CHECK from 'source/assets/check.png'

export default function Checkbox({ checked, setChecked }) {
    return (
        checked ?
            <View style={styles.container}>
                <Image
                    source={CHECK}
                    style={styles.img}
                />
            </View>
        :    
            <View style={[styles.container, styles.unchecked]} />
    )
}
