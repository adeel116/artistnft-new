import { StyleSheet } from 'react-native'
import theme from 'source/constants/colors'

export default StyleSheet.create({
    contentContainer: {
        flex: 1,
        backgroundColor: "rgba(56, 56, 56, 0.9)",
    },
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: theme.primary,
        borderBottomRightRadius: 45,
        borderTopRightRadius: 45,
    },
    logo: {
        width: 100,
        height: 100,
        marginTop: 50,
    },
    appName: {
        height: 20,
        marginTop: 18,
        marginBottom: 30,
        resizeMode: 'contain',
    },
    navItem: {
        width: '100%',
        paddingTop: 15,
        paddingLeft: 40,
        paddingBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    icon: {
        width: 22,
        height: 22
    },
    title: {
        fontSize: 16,
        color: "white",
        marginLeft: 30,
        fontFamily: 'Sansation_Bold'
    }
});