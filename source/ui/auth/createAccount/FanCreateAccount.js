import React, { useState } from 'react';
import { View, Image, Text, ScrollView, TouchableWithoutFeedback } from 'react-native';
import styles from './styles';

import APP_NAME from 'source/assets/app_name.png';
import Button from 'source/common/button';
import PhoneInput from 'source/common/phoneInput';
import Input from 'source/common/input';
import Checkbox from 'source/common/checkbox';
import { useDispatch } from 'react-redux';

export default function FanCreateAccount({ navigation }) {
    const dispatch = useDispatch()
    const [isChecked, setIsChecked] = useState(false)
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [countryCode, setCountryCode] = useState('1')
    const [password, setPassword] = useState('')
    const [country, setCountry] = useState('')
    const [PRO, setPRO] = useState('')
    const [merchableProfile, setMerchableProfile] = useState('')

    return (
        <ScrollView>
            <View style={styles.container}>
                
                <Image
                    source={APP_NAME}
                    style={styles.appName}
                />

                <Text style={styles.heading1}>SIGN UP</Text>
                <Text style={styles.heading2}>{`Please fill below details to create a\nnew account`}</Text>

                <Input
                    title='Name'
                    value={name}
                    onChangeText={setName}
                />

                <PhoneInput
                    value={phone}
                    onChangeText={setPhone}
                    style={{ marginTop: 10 }}
                    setMyCountryCode={setCountryCode}
                    title={`Performing Right\nOrganization No.`}
                />

                <Input
                    isPassword
                    title='Password'
                    value={password}
                    onChangeText={setPassword}
                />
                
                <Input
                    title='Country'
                    value={country}
                    onChangeText={setCountry}
                />
                
                <Input
                    value={PRO}
                    onChangeText={setPRO}
                    title='Performing Right Organization (PRO)'
                />
                
                <Input
                    value={merchableProfile}
                    onChangeText={setMerchableProfile}
                    title='Create/Add Merchabar Profile'
                />


                <TouchableWithoutFeedback onPress={()=>setIsChecked(prev => !prev)}>
                    <View style={styles.checkRow}>
                        <Checkbox
                            checked={isChecked}
                            setChecked={setIsChecked}
                        />
                        <Text style={[styles.heading3]}>I Agree to the Terms & Conditions</Text>
                    </View>
                </TouchableWithoutFeedback>


                <View style={{ marginTop: 20, width: '100%', paddingBottom: 15 }}>
                    <Button
                        title="NEXT"
                        onPress={()=>dispatch({ type: "AUTHENTICATE_APP" })}
                        style={{ marginLeft: 'auto', marginRight: 'auto' }}
                    />
                </View>
            </View>
        </ScrollView>
    )
}