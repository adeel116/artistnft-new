import React, { useState } from 'react';
import { View, ActivityIndicator, Image, Text, ScrollView, TouchableWithoutFeedback } from 'react-native';
import styles from './styles';

import IMAGE from 'source/assets/splash2.png';
import APP_NAME from 'source/assets/app_name.png';
import Dots from './components/Dots';
import Button from 'source/common/button';
import LanguageSelector from 'source/common/languageSelector';
import { useDispatch, useSelector } from 'react-redux';
import { saveRegisterData } from 'source/redux/actions/auth';

export default function Splash({ navigation }) {
    const dispatch = useDispatch()
    const {loading, roles} = useSelector(({utils}) => utils)
    const [toggler, setToggler] = useState(false)

    const nextHandler = (role) => {
        let selectedRole = roles.find(r => r.name === role);
        dispatch(saveRegisterData({
            role: selectedRole._id
        }))
        
        navigation.navigate('CreateAccountUsing')
    }

    return (
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
            <TouchableWithoutFeedback onPress={() => setToggler(Math.random())}>
                <View style={styles.container}>
                    <LanguageSelector toggler={toggler} />
                    
                    <Image
                        source={APP_NAME}
                        style={styles.appName}
                    />

                    <Image
                        source={IMAGE}
                        style={styles.image}
                    />

                    <Text style={styles.heading1}>{'CREATE ACCOUNT'}</Text>

                    <Text style={styles.heading2}>{'Please choose if you would like to\nsigup as a Artist or a Fan'}</Text>

                    <Dots active={1} />

                    {
                        loading ?
                            <ActivityIndicator color="#fff" style={{ marginTop: 'auto', marginBottom: 'auto' }} />
                        :
                            <View style={{ marginTop: 'auto', width: '100%', paddingBottom: 15 }}>
                                <Button
                                    style={{ marginTop: 50, marginLeft: 'auto', marginRight: 'auto' }}
                                    title="AS AN ARTIST"
                                    onPress={()=>nextHandler('doctor')}
                                />
                                
                                <Button
                                    title="AS A FAN"
                                    style={{ marginTop: 15, marginLeft: 'auto', marginRight: 'auto' }}
                                    onPress={()=>nextHandler('patient')}
                                />
                            </View>
                    }
                </View>
            </TouchableWithoutFeedback>
        </ScrollView>
    )
}