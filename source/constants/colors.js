export default {
    primary: "#383838",
    secondary: "#EA4836",
    dots: "#EB2F6B",
    grey: "#777777",
    phone: "#D03928",
} 